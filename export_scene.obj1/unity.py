import bpy
op = bpy.context.active_operator

op.filepath = 'D:\\New Tank Game\\untitled.obj'
op.use_selection = True
op.use_animation = False
op.use_mesh_modifiers = False
op.use_edges = True
op.use_smooth_groups = False
op.use_smooth_groups_bitflags = False
op.use_normals = True
op.use_uvs = False
op.use_materials = False
op.use_triangles = True
op.use_nurbs = False
op.use_vertex_groups = False
op.use_blen_objects = True
op.group_by_object = False
op.group_by_material = False
op.keep_vertex_order = True
op.global_scale = 1.0
op.path_mode = 'AUTO'
op.axis_forward = 'Y'
op.axis_up = 'Z'
