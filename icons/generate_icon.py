import bpy
import os
import sys
import ast

env_name = sys.argv[0]
name = sys.argv[1]
resolutions = list(ast.literal_eval(sys.argv[2]))

scene = bpy.context.scene
render = scene.render
print(render.resolution_x)
print(render.resolution_y)

layers = [layer.name for layer in scene.view_layers]
is_layers_divided_by_resolutions = False
for resolution in resolutions:
    if any(str(resolution) in layer_name for layer_name in layers):
        is_layers_divided_by_resolutions = True
        break

resolutions_dict = {}
if is_layers_divided_by_resolutions:
    for resolution in resolutions:
        for layer_name in layers:
            if str(resolution) in layer_name:
                resolutions_dict[resolution] = layer_name

print(resolutions_dict)

bpy.context.scene.render.filepath = os.path.join('C:/Users/Pavel/Desktop', ('render%d.jpg' % 1))
bpy.ops.render.render(write_still=True)
