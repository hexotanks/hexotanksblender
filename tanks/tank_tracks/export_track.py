import sys
from tank_tracks import prepare_objects
import obj_export
import order_obj_export
import helper

env_name = sys.argv[0]
tank_name = sys.argv[1]

track_l = "Track_L"
track_r = "Track_R"

prepare_objects.prepare_obj(track_l)
prepare_objects.prepare_obj(track_r)

obj_export.obj_export(tank_name, track_l, env_name)
obj_export.obj_export(tank_name, track_r, env_name)

order_obj_export.order_obj_export(tank_name, track_l, env_name)
order_obj_export.order_obj_export(tank_name, track_r, env_name)

helper.remove_exported_file(tank_name, track_l, 'obj', env_name)
helper.remove_exported_file(tank_name, track_r, 'obj', env_name)
