import mathutils
import helper


class TrackCurve:
        
    def __init__(self, obj1):
        self.obj = obj1
        
        self.coords = [(self.obj.matrix_world @ v.co) for v in self.obj.data.vertices]
        min_x = min(self.coords, key=lambda coord: coord.x)
        max_x = max(self.coords, key=lambda coord: coord.x)
        min_y = min(self.coords, key=lambda coord: coord.y)
        max_y = max(self.coords, key=lambda coord: coord.y)
        min_z = min(self.coords, key=lambda coord: coord.z)
        max_z = max(self.coords, key=lambda coord: coord.z)
        avg_x = (min_x.x + max_x.x) / 2
        avg_y = (min_y.y + max_y.y) / 2
        avg_z = (min_z.z + max_z.z) / 2
        self.center = mathutils.Vector((avg_x, avg_y, avg_z))
        
        self.vec_x = mathutils.Vector((1.0, 0.0, 0.0))
        self.vec_y = mathutils.Vector((0.0, 1.0, 0.0))
        self.vec_z = mathutils.Vector((0.0, 0.0, 1.0))
        
        self.total_distance = (self.coords[0] - self.coords[len(self.coords) - 1]).length
        for i in range(len(self.coords) - 1):
            self.total_distance += (self.coords[i + 1] - self.coords[i]).length
        
    def get_creation_loc_and_rot_by_distance_between_links(self, link_width, distance_between_links):
        result = []
        v1 = self.coords[0]
        next_ind = 1
        v2 = self.coords[next_ind]
        
        current_distance = 0.0
        
        for i in range(100):
            if (v2 - v1).length >= current_distance + link_width / 2:
                loc = ((current_distance + link_width / 2) / (v2 - v1).length) * (v2 - v1) + v1
                rot_x = 0.0
                rot_y = helper.angle_between((v2 - v1), self.vec_y)
                if loc.y < self.center.y:
                    rot_y *= -1.0
                rot_z = 0.0
                rot = (rot_y, rot_x, rot_z)
                result.append((loc, rot))
                current_distance += link_width + distance_between_links
            else:
                current_distance = -1.0 * (link_width - (current_distance + link_width - (v2 - v1).length))
                v1 = v2
                next_ind += 1
                if next_ind >= len(self.coords):
                    next_ind = 0
                if next_ind == 1:
                    break
                
                v2 = self.coords[next_ind]
                
                loc = ((current_distance + link_width / 2) / (v2 - v1).length) * (v2 - v1) + v1
                rot_x = 0.0
                rot_y = helper.angle_between((v2 - v1), self.vec_y)
                if loc.y < self.center.y:
                    rot_y *= -1.0
                rot_z = 0.0
                rot = (rot_y, rot_x, rot_z)
                result.append((loc, rot))
                current_distance += link_width + distance_between_links
        
        return result

    def get_creation_loc_and_rot_by_links_count(self, link_width, links_count):
        result = []
        v1 = self.coords[0]
        prev_ind = 0
        next_ind = 1
        v2 = self.coords[next_ind]
        
        distance_between_links = (self.total_distance / links_count) - link_width
        
        current_distance = 0.0
        
        for i in range(links_count):
            if (v2 - v1).length >= current_distance + link_width / 2:
                loc = ((current_distance + link_width / 2) / (v2 - v1).length) * (v2 - v1) + v1
                rot_x = 0.0
                rot_y = helper.angle_between((v2 - v1), self.vec_y)
                if v1.z <= v2.z:
                    rot_y = 0 + rot_y
                elif v1.z > v2.z:
                    rot_y = 0 - rot_y
                rot_z = 0.0
                rot1 = (rot_y, rot_x, rot_z)
                rot2 = (-rot_y, rot_x, rot_z)
                result.append((loc, rot1, prev_ind, next_ind, rot2))
                current_distance += link_width + distance_between_links
            else:
                current_distance = -1.0 * (link_width - (current_distance + link_width - (v2 - v1).length))
                v1 = v2
                prev_ind += 1
                next_ind += 1
                if next_ind >= len(self.coords):
                    next_ind = 0
                if next_ind == 1:
                    break
                
                v2 = self.coords[next_ind]
                
                loc = ((current_distance + link_width / 2) / (v2 - v1).length) * (v2 - v1) + v1
                rot_x = 0.0
                rot_y = helper.angle_between((v2 - v1), self.vec_y)
                if v1.z <= v2.z:
                    rot_y = 0 + rot_y
                elif v1.z > v2.z:
                    rot_y = 0 - rot_y
                rot_z = 0.0
                rot1 = (rot_y, rot_x, rot_z)
                rot2 = (-rot_y, rot_x, rot_z)
                result.append((loc, rot1, prev_ind, next_ind, rot2))
                current_distance += link_width + distance_between_links
        
        return result
