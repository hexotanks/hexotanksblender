import bpy
import sys
from tank_tracks.track_curve import TrackCurve
import helper
import obj_export
import track_spec_export

env_name = sys.argv[0]
tank_name = sys.argv[1]
track_links_count = int(sys.argv[2])
track_link_width = float(sys.argv[3])

track_curve_l = "TrackCurve_L"
track_curve_r = "TrackCurve_R"
track_coll_name_l = "Track_L"
track_coll_name_r = "Track_R"
track_temp_name = "Track_Temp"
track_temp = bpy.context.scene.objects[track_temp_name]
track_l_name = "Track_L"
track_r_name = "Track_R"
target_track_link_name = "TrackLink"
track_link_num = 0


def copy_track_link(temp_obj, collection_name, loc, rot):
    global track_link_num
    track_link_num += 1
    new_obj = temp_obj.copy()
    new_obj.data = temp_obj.data.copy()
    bpy.data.collections[collection_name].objects.link(new_obj)
    new_obj.name = target_track_link_name + str(track_link_num)
    new_obj.data.name = target_track_link_name + str(track_link_num)
    new_obj.location = loc
    new_obj.rotation_euler = rot
    return new_obj


def generate_track_links(curve_name, target_collection_name):
    track_curve = bpy.context.scene.objects[curve_name]
    track_curve = TrackCurve(track_curve)
    res = track_curve.get_creation_loc_and_rot_by_links_count(track_link_width, track_links_count)
    for r in res:
        copy_track_link(track_temp, target_collection_name, r[0], r[1])
        print(helper.rad2deg(r[1]))
    return res


def join_track_links(link_from_num, link_to_num, joined_name):
    bpy.ops.object.select_all(action='DESELECT')
    for i in range(link_from_num, link_to_num):
        bpy.data.objects[target_track_link_name + str(i)].select_set(True)
    bpy.context.view_layer.objects.active = bpy.data.objects[target_track_link_name + str(link_from_num)]
    bpy.ops.object.join()
    track = bpy.context.scene.objects[target_track_link_name + str(link_from_num)]
    track.name = joined_name
    track.data.name = joined_name
    bpy.ops.object.select_all(action='DESELECT')


def export_track_spec(generated_track_links_data, target='l'):
    bpy.ops.object.select_all(action='DESELECT')
    if target == 'l':
        obj_export.obj_export(tank_name, track_temp_name, env_name)
        track_spec_export.track_spec_export(tank_name, track_temp_name, target_track_link_name, track_l_name, generated_track_links_data, 1, track_links_count + 1, env_name)
        helper.remove_exported_file(tank_name, track_temp_name, 'obj', env_name)
    elif target == 'r':
        obj_export.obj_export(tank_name, track_temp_name, env_name)
        track_spec_export.track_spec_export(tank_name, track_temp_name, target_track_link_name, track_r_name, generated_track_links_data, track_links_count + 1, (track_links_count * 2) + 1, env_name)
        helper.remove_exported_file(tank_name, track_temp_name, 'obj', env_name)
    bpy.ops.object.select_all(action='DESELECT')


def generate_track(target='l'):
    bpy.ops.object.select_all(action='DESELECT')
    if target == 'l':
        helper.deep_remove_collection(track_coll_name_l)
        helper.create_collection_in_root(track_coll_name_l)
        res = generate_track_links(track_curve_l, track_coll_name_l)
        export_track_spec(res, 'l')
        join_track_links(1, track_links_count + 1, track_l_name)
    elif target == 'r':
        helper.deep_remove_collection(track_coll_name_r)
        helper.create_collection_in_root(track_coll_name_r)
        res = generate_track_links(track_curve_r, track_coll_name_r)
        export_track_spec(res, 'r')
        join_track_links(track_links_count + 1, (track_links_count * 2) + 1, track_r_name)
    bpy.ops.object.select_all(action='DESELECT')


generate_track('l')
generate_track('r')
