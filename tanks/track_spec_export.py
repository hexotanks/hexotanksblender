import bpy
import helper
import exporters


def track_spec_export(tank_name, obj_name, target_track_link_name, target_obj_name, generated_track_links_data, link_from_num, link_to_num, env_name):
    target_filepath_spec = helper.get_target_specs_filepath(tank_name, target_obj_name, 'txt', env_name)
    target_filepath_obj = helper.get_target_filepath(tank_name, obj_name, 'obj', env_name)

    bpy.ops.object.select_all(action='DESELECT')

    obj = bpy.context.scene.objects[obj_name]
    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj

    exporters.export_track_spec(obj, target_track_link_name, generated_track_links_data, link_from_num, link_to_num, target_filepath_spec, target_filepath_obj)

    bpy.ops.object.select_all(action='DESELECT')
