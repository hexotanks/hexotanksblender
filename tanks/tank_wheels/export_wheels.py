import sys
from tank_wheels import prepare_objects
import obj_export
import order_obj_export
import helper

env_name = sys.argv[0]
tank_name = sys.argv[1]

wheels_l = "Wheels_L"
wheels_r = "Wheels_R"

prepare_objects.prepare_obj(wheels_l)
prepare_objects.prepare_obj(wheels_r)

obj_export.obj_export(tank_name, wheels_l, env_name)
obj_export.obj_export(tank_name, wheels_r, env_name)

order_obj_export.order_obj_export(tank_name, wheels_l, env_name)
order_obj_export.order_obj_export(tank_name, wheels_r, env_name)

helper.remove_exported_file(tank_name, wheels_l, 'obj', env_name)
helper.remove_exported_file(tank_name, wheels_r, 'obj', env_name)
