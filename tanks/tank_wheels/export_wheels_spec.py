import sys
import wheels_spec_export
import obj_export
import helper

env_name = sys.argv[0]
tank_name = sys.argv[1]

wheels_l = "Wheels_L"
wheels_r = "Wheels_R"

obj_export.obj_export(tank_name, wheels_l, env_name)
obj_export.obj_export(tank_name, wheels_r, env_name)

wheels_spec_export.wheels_spec_export(tank_name, wheels_l, env_name)
wheels_spec_export.wheels_spec_export(tank_name, wheels_r, env_name)

helper.remove_exported_file(tank_name, wheels_l, 'obj', env_name)
helper.remove_exported_file(tank_name, wheels_r, 'obj', env_name)
