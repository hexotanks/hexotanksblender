import numpy as np
import copy
import helper

from order_obj.f import F
from order_obj.v import V
from order_obj.vn import VN
from order_obj.vvn import VVN


def read_obj_file(tank_name, obj_name, env_name):
    target_filepath = helper.get_target_filepath(tank_name, obj_name, 'obj', env_name)
    return read_obj_file_by_path(target_filepath)


def read_obj_file_by_path(target_filepath):
    file = open(target_filepath, 'r')
    lines = file.readlines()
    file.close()

    v = []
    vn = []
    f = []
    v_vn = []
    f_lines = []

    v_count = 1
    vn_count = 1
    for line in lines:

        if line.startswith('v '):
            split_line = line.strip().split(' ')
            x = split_line[1]
            y = split_line[2]
            z = split_line[3]
            v.append(V(v_count, x, y, z))
            v_count += 1
        if line.startswith('vn '):
            split_line = line.strip().split(' ')
            x = split_line[1]
            y = split_line[2]
            z = split_line[3]
            vn.append(VN(vn_count, x, y, z))
            vn_count += 1
        if line.startswith('f '):
            f_lines.append(line)

    for line in f_lines:
        split_line = line.strip().split(' ')
        el1 = split_line[1].split('//')
        el2 = split_line[2].split('//')
        el3 = split_line[3].split('//')
        v_vn.append((int(el1[0]), int(el1[1])))
        v_vn.append((int(el2[0]), int(el2[1])))
        v_vn.append((int(el3[0]), int(el3[1])))

    # fix normals round
    replace_normals_list = []
    for x in range(len(vn)):
        for y in range(len(vn)):
            if x != y:
                x_equal = abs(vn[x].x - vn[y].x) <= 0.00011
                y_equal = abs(vn[x].y - vn[y].y) <= 0.00011
                z_equal = abs(vn[x].z - vn[y].z) <= 0.00011
                if x_equal and y_equal and z_equal:
                    replace_normals_list.append((x + 1, y + 1))

    for x in range(len(v_vn)):
        for nor in replace_normals_list:
            if v_vn[x][1] == nor[0]:
                v_vn[x] = (v_vn[x][0], nor[1])

    result_v_vn = v_vn.copy()
    ###

    v_vn_tuples = [vvn for vvn in v_vn]
    v_vn_tuples_list = np.unique(v_vn_tuples, axis=0)

    uniq_v_vn_tuples = []
    for i in v_vn_tuples_list:
        uniq_v_vn_tuples.append(tuple(i))

    v_vn = []
    for v_vn_tuples in uniq_v_vn_tuples:
        v1 = next((vert for vert in v if vert.ind == int(v_vn_tuples[0])), None)
        n1 = next((vert for vert in vn if vert.ind == int(v_vn_tuples[1])), None)
        v_vn.append(VVN(copy.deepcopy(v1), copy.deepcopy(n1)))

    for i in range(0, len(result_v_vn), 3):
        v_vn1 = next((vvn for vvn in v_vn if vvn.v.ind == int(result_v_vn[i][0]) and vvn.n.ind == int(result_v_vn[i][1])), None)
        v_vn2 = next((vvn for vvn in v_vn if vvn.v.ind == int(result_v_vn[i + 1][0]) and vvn.n.ind == int(result_v_vn[i + 1][1])), None)
        v_vn3 = next((vvn for vvn in v_vn if vvn.v.ind == int(result_v_vn[i + 2][0]) and vvn.n.ind == int(result_v_vn[i + 2][1])), None)
        f.append(F(v_vn1, v_vn2, v_vn3))

    return v, vn, f, v_vn


def transform_obj_data(v, vn, f, v_vn):

    res_v = []
    res_vn = []
    res_f = []

    v_ind = 0
    for i in range(len(v_vn)):
        if v_vn[i].v.ind != v_ind:
            res_v.append(v_vn[i].v)
            res_vn.append(v_vn[i].n)
            v_ind = v_vn[i].v.ind
        else:
            res_v.append(v_vn[i].v)
            res_vn.append(v_vn[i].n)
            for vvn in v_vn[i:]:
                vvn.v.new_ind += 1

    res_f = f[:]

    return res_v, res_vn, res_f
