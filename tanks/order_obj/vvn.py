class VVN:

    def __init__(self, v, n):
        self.v = v
        self.n = n

    def to_tuple(self):
        return self.v.ind, self.n.ind

    def __str__(self):
        return "{} {}".format(self.v.ind, self.n.ind)
