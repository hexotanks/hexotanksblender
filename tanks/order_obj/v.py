class V:

    def __init__(self, ind, x, y, z):
        self.ind = int(ind)
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)
        self.new_ind = int(ind)

    def __str__(self):
        return "{} {} {} {}".format(self.ind, self.x, self.y, self.z)
