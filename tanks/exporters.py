import bpy
import os
import helper
from order_obj import order_obj_logic


def export_fbx(preset, target_filepath):
    filename = "_".join(s.lower() for s in preset.split())
    preset_path = bpy.utils.preset_paths("operator/export_scene.fbx/")

    if preset_path:
        filepath = os.path.join(preset_path[0], filename + '.py')
        if filepath:
            class Container(object):
                __slots__ = ('__dict__',)

            op = Container()
            file = open(filepath, 'r')

            for line in file.readlines()[3::]:
                if line.startswith('op.filepath'):
                    index = line.index("=") + 2
                    line = line[:index] + '\'' + target_filepath + '\''
                    print(line[index::])
                exec(line, globals(), locals())

            kwargs = op.__dict__
            bpy.ops.export_scene.fbx(**kwargs)

        else:
            raise FileNotFoundError(preset_path[0] + filename + '.py')


def export_curve(obj, target_filepath):
    coords = [(obj.matrix_world @ v.co) for v in obj.data.vertices]
    coords = [(v[0], v[2], v[1]) for v in coords]

    rotated_coords = []
    for coord in coords:
        rotated_coord = helper.rotate_point(coord, (0.0, 180.0, 0.0))
        rotated_coords.append(rotated_coord)

    import bmesh

    mesh = bmesh.new()
    mesh.from_mesh(obj.data)

    with open(target_filepath, 'w') as file:
        file.write('{} {}'.format('o', obj.name + '\n'))

        for c in rotated_coords:
            file.write('{} {} {} {}{}'.format('v', c[0], c[1], c[2], '\n'))

        for edge in mesh.edges:
            v1 = edge.verts[0].index + 1
            v2 = edge.verts[1].index + 1
            file.write('{} {} {}{}'.format('l', v1, v2, '\n'))


def export_tank_spec(obj, tank_specs, target_filepath):
    obj_loc = obj.location
    rotated_obj_loc = helper.rotate_point((obj_loc.x, obj_loc.z, obj_loc.y), (0.0, 180.0, 0.0))

    with open(target_filepath, 'w') as file:
        file.write('{} {} {} {} {} {} {} {} {}{}'.format('v', rotated_obj_loc[0], rotated_obj_loc[1], rotated_obj_loc[2], tank_specs[0], tank_specs[1], tank_specs[2], tank_specs[3], tank_specs[4], '\n'))


def export_obj(preset, target_filepath):
    filename = "_".join(s.lower() for s in preset.split())
    preset_path = bpy.utils.preset_paths("operator/export_scene.obj/")

    if preset_path:
        filepath = os.path.join(preset_path[0], filename + '.py')
        if filepath:
            class Container(object):
                __slots__ = ('__dict__',)

            op = Container()
            file = open(filepath, 'r')

            for line in file.readlines()[3::]:
                if line.startswith('op.filepath'):
                    index = line.index("=")+2
                    line = line[:index] + '\'' + target_filepath + '\''
                    print(line[index::])
                exec(line, globals(), locals())

            kwargs = op.__dict__
            bpy.ops.export_scene.obj(**kwargs)

        else:
            raise FileNotFoundError(preset_path[0] + filename + '.py')


def export_order_obj(tank_name, obj_name, env_name, target_filepath):
    v, vn, f, v_vn = order_obj_logic.read_obj_file(tank_name, obj_name, env_name)
    v, vn, f = order_obj_logic.transform_obj_data(v, vn, f, v_vn)

    with open(target_filepath, 'w') as file:
        file.write('{} {}'.format('o', obj_name + '\n'))
        for vec in v:
            rotated_vec = helper.rotate_vector(vec, (-90.0, 0.0, 0.0))
            file.write('{} {} {} {}{}'.format('v', -rotated_vec.x, rotated_vec.y, rotated_vec.z, '\n'))

        for vec in vn:
            rotated_vec = helper.rotate_vector(vec, (-90.0, 0.0, 0.0))
            file.write('{} {} {} {}{}'.format('vn', -rotated_vec.x, rotated_vec.y, rotated_vec.z, '\n'))

        for face in f:
            file.write('{} {} {} {}{}'.format('f', face.v_vn3.v.new_ind - 1, face.v_vn2.v.new_ind - 1, face.v_vn1.v.new_ind - 1, '\n'))


def export_wheels_spec(obj, target_filepath_spec, target_filepath_obj):
    v, vn, f, v_vn = order_obj_logic.read_obj_file_by_path(target_filepath_obj)
    islands_num, islands_vert_indexes = helper.count_islands(obj)

    with open(target_filepath_spec, 'w') as file:
        cur_start = 0
        for i in range(len(islands_vert_indexes)):
            center = helper.calc_vertices_center(obj, cur_start, cur_start + len(islands_vert_indexes[i]))
            radius = helper.calc_obj_radius(obj, cur_start, cur_start + len(islands_vert_indexes[i]))
            vertices_count = len([vvn for vvn in v_vn if cur_start + 1 <= vvn.v.ind <= cur_start + len(islands_vert_indexes[i])])
            file.write('{} {} {} {} {} {}{}'.format('w', vertices_count, center.x, center.z, -center.y, radius, '\n'))
            cur_start += len(islands_vert_indexes[i])


def export_track_spec(track_link_obj, target_track_link_name, generated_track_links_data, link_from_num, link_to_num, target_filepath_spec, target_filepath_obj):
    v, vn, f, v_vn = order_obj_logic.read_obj_file_by_path(target_filepath_obj)
    islands_num, islands_vert_indexes = helper.count_islands(track_link_obj)
    vertices_count = len([vvn for vvn in v_vn if 1 <= vvn.v.ind <= len(islands_vert_indexes[0])])

    with open(target_filepath_spec, 'w') as file:
        file.write('{} {}{}'.format('c', vertices_count, '\n'))

        track_link_count = 1
        for i in range(link_from_num, link_to_num):
            track = bpy.data.objects[target_track_link_name + str(i)]
            sign = helper.sign(helper.rad2deg(generated_track_links_data[track_link_count - 1][1][0]))
            rot = (180 - abs(helper.rad2deg(generated_track_links_data[track_link_count - 1][1][0]))) * sign
            file.write('{} {} {} {} {} {} {}{}'.format('o', -track.location.x, track.location.z, -track.location.y,
                                                       generated_track_links_data[track_link_count - 1][2], generated_track_links_data[track_link_count - 1][3], rot, '\n'))
            track_link_count += 1
