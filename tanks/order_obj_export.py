import helper
import exporters


def order_obj_export(tank_name, obj_name, env_name):
    target_filepath = helper.get_target_filepath(tank_name, obj_name, 'orobj', env_name)
    exporters.export_order_obj(tank_name, obj_name, env_name, target_filepath)
