import bpy
import helper
import exporters


def tank_spec_export(tank_name, obj_name, env_name, tank_specs):
    target_filepath = helper.get_target_specs_filepath(tank_name, "Tank", 'txt', env_name)

    bpy.ops.object.select_all(action='DESELECT')

    obj = bpy.context.scene.objects[obj_name]
    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj

    exporters.export_tank_spec(obj, tank_specs, target_filepath)

    bpy.ops.object.select_all(action='DESELECT')
