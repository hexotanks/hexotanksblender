import sys
from tank_body import prepare_objects
import fbx_export

env_name = sys.argv[0]
tank_name = sys.argv[1]

body_name = "Body"
gun_name = "Gun"
mantlet_name = "Mantlet"
turret_name = "Turret"

prepare_objects.prepare_obj(body_name)
prepare_objects.prepare_obj2(gun_name)
prepare_objects.prepare_obj2(mantlet_name)
prepare_objects.prepare_obj2(turret_name)

fbx_export.fbx_export(tank_name, body_name, env_name)
fbx_export.fbx_export(tank_name, turret_name, env_name)
fbx_export.fbx_export(tank_name, gun_name, env_name)
fbx_export.fbx_export(tank_name, mantlet_name, env_name)
