import sys
from tank_body import prepare_objects
import tank_spec_export
import ast

env_name = sys.argv[0]
tank_name = sys.argv[1]
tank_specs = list(ast.literal_eval(sys.argv[4]))

shoot_point_name = "Shoot_Point"

prepare_objects.prepare_obj2(shoot_point_name)

tank_spec_export.tank_spec_export(tank_name, shoot_point_name, env_name, tank_specs)
