import bpy
import numpy as np


def prepare_obj(name):
    bpy.ops.object.select_all(action='DESELECT')

    obj = bpy.context.scene.objects[name]
    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj
    bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
    bpy.context.scene.cursor.location = (0.0, 0.0, 0.0)
    bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='BOUNDS')

    angle90 = np.deg2rad(-90)
    rot90 = (angle90, 0.0, 0.0)
    angle_neg90 = np.deg2rad(90)
    rot0 = (angle_neg90, 0.0, 0.0)

    obj.rotation_euler = rot90
    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
    obj.rotation_euler = rot0

    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.normals_make_consistent(inside=False)
    bpy.ops.object.editmode_toggle()

    bpy.ops.object.select_all(action='DESELECT')


def prepare_obj2(name):
    bpy.ops.object.select_all(action='DESELECT')

    obj = bpy.context.scene.objects[name]
    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj
    bpy.ops.object.transform_apply(location=False, rotation=True, scale=True)

    angle90 = np.deg2rad(-90)
    rot90 = (angle90, 0.0, 0.0)
    angle_neg90 = np.deg2rad(90)
    rot0 = (angle_neg90, 0.0, 0.0)

    obj.rotation_euler = rot90
    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
    obj.rotation_euler = rot0

    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.normals_make_consistent(inside=False)
    bpy.ops.object.editmode_toggle()

    bpy.ops.object.select_all(action='DESELECT')
