import sys
from tank_curve import prepare_objects
import curve_export

env_name = sys.argv[0]
tank_name = sys.argv[1]

track_curve_l = "TrackCurve_L"
track_curve_r = "TrackCurve_R"

prepare_objects.prepare_obj(track_curve_l)
prepare_objects.prepare_obj(track_curve_r)

curve_export.curve_export(tank_name, track_curve_l, env_name)
curve_export.curve_export(tank_name, track_curve_r, env_name)
