import bpy


def prepare_obj(name):
    bpy.ops.object.select_all(action='DESELECT')

    obj = bpy.context.scene.objects[name]
    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj
    bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
    bpy.context.scene.cursor.location = (0.0, 0.0, 0.0)
    bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='BOUNDS')

    bpy.ops.object.select_all(action='DESELECT')
