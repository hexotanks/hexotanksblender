import bpy
import helper
import exporters


def curve_export(tank_name, obj_name, env_name):
    target_filepath = helper.get_target_filepath(tank_name, obj_name, 'curve', env_name)

    bpy.ops.object.select_all(action='DESELECT')

    obj = bpy.context.scene.objects[obj_name]
    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj

    exporters.export_curve(obj, target_filepath)

    bpy.ops.object.select_all(action='DESELECT')
