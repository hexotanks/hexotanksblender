import os
import numpy as np
import bpy
import mathutils
import math


def get_target_filepath(tank_name, obj_name, extension, env_name):
    folder = os.path.join(r'D:\project_tanks\{}\models\tanks\exported\tanks'.format(env_name), tank_name)
    if not os.path.exists(folder):
        os.makedirs(folder)
    return os.path.join(folder, obj_name + '.' + extension).replace("\\", "\\\\")


def get_target_specs_filepath(tank_name, obj_name, extension, env_name):
    folder = os.path.join(r'D:\project_tanks\{}\models\tanks\exported\tanks'.format(env_name), tank_name)
    folder = os.path.join(folder, "specs")
    if not os.path.exists(folder):
        os.makedirs(folder)
    return os.path.join(folder, obj_name + '_spec.' + extension).replace("\\", "\\\\")


def unit_vector(vector):
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def angle_between_in_degrees(v1, v2):
    return np.rad2deg(angle_between(v1, v2))


def rad2deg(angle):
    return np.rad2deg(angle)


def rotate_point(point, angle_vec_deg):
    vec = mathutils.Vector(point)
    angle_vec_rad = (math.radians(angle_vec_deg[0]), math.radians(angle_vec_deg[1]), math.radians(angle_vec_deg[2]))
    eul = mathutils.Euler(angle_vec_rad, 'XYZ')
    vec.rotate(eul)
    return vec


def rotate_vector(v, angle_vec_deg):
    vec = mathutils.Vector((v.x, v.y, v.z))
    angle_vec_rad = (math.radians(angle_vec_deg[0]), math.radians(angle_vec_deg[1]), math.radians(angle_vec_deg[2]))
    eul = mathutils.Euler(angle_vec_rad, 'XYZ')
    vec.rotate(eul)
    return vec


def deep_remove_collection(collection_name):
    coll = bpy.data.collections.get(collection_name)

    if coll:
        obs = [o for o in coll.objects if o.users == 1]
        while obs:
            bpy.data.objects.remove(obs.pop())
        bpy.data.collections.remove(coll)


def create_collection_in_root(collection_name):
    bpy.ops.collection.create(name=collection_name)
    bpy.context.scene.collection.children.link(bpy.data.collections[collection_name])


def remove_exported_file(tank_name, obj_name, extension, env_name):
    target_filepath = get_target_filepath(tank_name, obj_name, extension, env_name)
    os.remove(target_filepath)


def make_vert_paths(verts, edges):
    result = {v.index: set() for v in verts}
    for e in edges:
        result[e.vertices[0]].add(e.vertices[1])
        result[e.vertices[1]].add(e.vertices[0])
    return result


def follow_edges(starting_index, paths):
    current = [starting_index]

    vertices_indexes = []

    follow = True
    while follow:
        eligible = set([ind for ind in current if ind in paths])
        if len(eligible) == 0:
            follow = False
        else:
            vertices_indexes.extend(eligible)
            next = [paths[i] for i in eligible]
            for key in eligible: paths.pop(key)
            current = set([ind for sub in next for ind in sub])

    return vertices_indexes


def count_islands(obj):
    paths = make_vert_paths(obj.data.vertices, obj.data.edges)
    objects_vert_count = []
    found = True
    n = 0
    while found:
        try:
            starting_index = next(iter(paths.keys()))
            n = n + 1
            vertices_count = follow_edges(starting_index, paths)
            objects_vert_count.append(vertices_count)
        except:
            found = False
    return n, objects_vert_count


def count_islands_triangles(obj):
    paths = make_vert_paths(obj.data.vertices, obj.data.edges)
    objects_vert_count = []
    found = True
    n = 0
    while found:
        try:
            starting_index = next(iter(paths.keys()))
            n = n + 1
            vertices_count = follow_edges(starting_index, paths)
            objects_vert_count.append(vertices_count)
        except:
            found = False

    faces = obj.data.polygons
    faces_count = []
    for island in objects_vert_count:
        found_faces = []
        for target_vert_ind in island:
            for face in faces:
                if face in found_faces:
                    continue
                for vert in face.vertices:
                    if vert == target_vert_ind:
                        found_faces.append(face)
                        break
        faces_count.append(len(found_faces))

    return n, faces_count


def count_islands_triangles2(obj):
    paths = make_vert_paths(obj.data.vertices, obj.data.edges)
    objects_vert_count = []
    found = True
    n = 0
    while found:
        try:
            starting_index = next(iter(paths.keys()))
            n = n + 1
            vertices_count = follow_edges(starting_index, paths)
            objects_vert_count.append(vertices_count)
        except:
            found = False

    faces = obj.data.polygons
    faces_indexes_list = []
    for island in objects_vert_count:
        found_faces_ind = []
        for target_vert_ind in island:
            for i in range(len(faces)):
                if i in found_faces_ind:
                    continue
                for vert in faces[i].vertices:
                    if vert == target_vert_ind:
                        found_faces_ind.append(i)
                        break
        faces_indexes_list.append(found_faces_ind)

    return n, faces_indexes_list


def calc_vertices_center(obj, from_target_vertices, to_target_vertices):
    coords = [(obj.matrix_world @ v.co) for v in obj.data.vertices]
    target_coords = coords[from_target_vertices:to_target_vertices]
    min_x = min(target_coords, key=lambda coord: coord.x)
    max_x = max(target_coords, key=lambda coord: coord.x)
    min_y = min(target_coords, key=lambda coord: coord.y)
    max_y = max(target_coords, key=lambda coord: coord.y)
    min_z = min(target_coords, key=lambda coord: coord.z)
    max_z = max(target_coords, key=lambda coord: coord.z)
    avg_x = (min_x.x + max_x.x) / 2
    avg_y = (min_y.y + max_y.y) / 2
    avg_z = (min_z.z + max_z.z) / 2
    return mathutils.Vector((avg_x, avg_y, avg_z))


def calc_obj_radius(obj, from_target_vertices, to_target_vertices):
    coords = [(obj.matrix_world @ v.co) for v in obj.data.vertices]
    target_coords = coords[from_target_vertices:to_target_vertices]
    min_z = min(target_coords, key=lambda coord: coord.z)
    max_z = max(target_coords, key=lambda coord: coord.z)
    return abs(max_z.z - min_z.z) / 2


def sign(num):
    return np.sign(num)


