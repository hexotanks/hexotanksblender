import bpy
import os


def export_fbx(preset, target_filepath):
    filename = "_".join(s.lower() for s in preset.split())
    preset_path = bpy.utils.preset_paths("operator/export_scene.fbx/")

    if preset_path:
        filepath = os.path.join(preset_path[0], filename + '.py')
        if filepath:
            class Container(object):
                __slots__ = ('__dict__',)

            op = Container()
            file = open(filepath, 'r')

            for line in file.readlines()[3::]:
                if line.startswith('op.filepath'):
                    index = line.index("=") + 2
                    line = line[:index] + '\'' + target_filepath + '\''
                    print(line[index::])
                exec(line, globals(), locals())

            kwargs = op.__dict__
            bpy.ops.export_scene.fbx(**kwargs)

        else:
            raise FileNotFoundError(preset_path[0] + filename + '.py')
