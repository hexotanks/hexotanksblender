import sys
import prepare_objects
import fbx_export
import helper

env_name = sys.argv[0]

shell_prefix_name = "Shell_"

target_objects = helper.get_objects_by_prefix_name(shell_prefix_name)
for obj in target_objects:
    prepare_objects.prepare_obj(obj)
    fbx_export.fbx_export(obj.name, env_name)
