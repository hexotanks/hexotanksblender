import os
import bpy


def get_objects_by_prefix_name(prefix_name):
    obj_list = []
    objs = bpy.context.scene.objects
    for obj in objs:
        if obj.name.startswith(prefix_name):
            obj_list.append(obj)

    return obj_list


def get_target_filepath(obj_name, extension, env_name):
    folder = os.path.join(r'D:\project_tanks\{}\models\tanks\exported'.format(env_name), 'Shells')
    if not os.path.exists(folder):
        os.makedirs(folder)
    return os.path.join(folder, obj_name + '.' + extension).replace("\\", "\\\\")
